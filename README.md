#Group L Milestone Project

#Group Members

Jordan O'Donnell
Liam Noonan
Seamus Robinson

##Application Features

The project will produce a milestone planner application that will be able to show:
A description of the milestone
An intended due date
The actual completion date
Milestones can be removed from the list.
Milestones can be edited
A milestone(list)can be shared with friends using a link

##Startup

The project can be run be opening the main class which is the "Runner" class. From this it will create a server from which you can connect to the pages with localhost:9001/
