package Application.Model;

import org.junit.Test;
import static org.junit.Assert.*;

public class MilestoneTest {


    @Test
    public void testGetUserId() throws Exception {
       Milestone test = new Milestone("1","Title","Description","11/11/11","11/11/11","1");
       assertEquals("1",test.getUserId());
    }

    @Test
    public void testSetUserId() {
        Milestone test = new Milestone("1","Title","Description","11/11/11","11/11/11","1");
        test.setUserId("2");
        assertEquals("2",test.getUserId());
    }

    @Test
    public void testGetTitle() {
        Milestone test = new Milestone("1","Title","Description","11/11/11","11/11/11","1");
        assertEquals("Title",test.getTitle());
    }

    @Test
    public void testSetTitle() {
        Milestone test = new Milestone("1","Title","Description","11/11/11","11/11/11","1");
        test.setTitle("Title 2");
        assertEquals("Title 2",test.getTitle());
    }

    @Test
    public void testGetDescription() {
        Milestone test = new Milestone("1","Title","Description","11/11/11","11/11/11","1");
        assertEquals("Description",test.getDescription());
    }

    @Test
    public void testSetDescription() {
        Milestone test = new Milestone("1","Title","Description","11/11/11","11/11/11","1");
        test.setDescription("Description 2");
        assertEquals("Description 2",test.getDescription());
    }

    @Test
    public void testGetDueDate() {
        Milestone test = new Milestone("1","Title","Description","11/11/11","11/11/11","1");
        assertEquals("11/11/11",test.getDueDate());
    }

    @Test
    public void testSetDueDate() {
        Milestone test = new Milestone("1","Title","Description","11/11/11","11/11/11","1");
        test.setDueDate("12/12/12");
        assertEquals("12/12/12",test.getDueDate());
    }

    @Test
    public void testGetCompletionDate() {
        Milestone test = new Milestone("1","Title","Description","11/11/11","11/11/11","1");
        assertEquals("11/11/11",test.getCompletionDate());
    }

    @Test
    public void testSetCompletionDate() {
        Milestone test = new Milestone("1","Title","Description","11/11/11","11/11/11","1");
        test.setCompletionDate("12/12/12");
        assertEquals("12/12/12",test.getCompletionDate());
    }

    @Test
    public void testGetId() {
        Milestone test = new Milestone("1","Title","Description","11/11/11","11/11/11","1");
        assertEquals("1",test.getId());
    }
}