package Application.Servlet;


import Application.H2Milestone;
import Application.Model.Milestone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class EditMilestoneServlet extends BaseServlet {


    private static final String EDIT_MILESTONES_TEMPLATE = "edit.mustache";

    private final H2Milestone h2Milestone;


    public EditMilestoneServlet(H2Milestone h2Milestone) {
        this.h2Milestone = h2Milestone;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        String milestoneId = session.getAttribute("milestoneId").toString();
        Milestone milestone = h2Milestone.getMilestone((milestoneId));
        showView(response, EDIT_MILESTONES_TEMPLATE, milestone);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //System.out.println("Hello, I'm a post method");

        HttpSession session = request.getSession(true);
        String milestoneId = session.getAttribute("milestoneId").toString();

        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String duedate = request.getParameter("duedate");
        String completiondate = request.getParameter("completiondate");

        Milestone m = h2Milestone.getMilestone((milestoneId));
        m.setTitle(title);
        m.setDescription(description);
        m.setDueDate(duedate);
        m.setCompletionDate(completiondate);

        h2Milestone.updateMilestone(milestoneId,m);
        response.sendRedirect(response.encodeRedirectURL("/milestone"));
    }

}
