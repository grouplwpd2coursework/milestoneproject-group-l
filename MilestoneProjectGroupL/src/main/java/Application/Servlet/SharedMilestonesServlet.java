package Application.Servlet;


import Application.H2Milestone;
import Application.Model.Milestone;
import Application.Model.SharedUsername;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


public class SharedMilestonesServlet extends BaseServlet {

    public SharedMilestonesServlet(H2Milestone h2Milestone){
        this.h2Milestone = h2Milestone;
    }

    private static final String SHARED_MILESTONES_TEMPLATE = "shared.mustache";
    private static final String ID_PARAMETER = "shareId";

    private final H2Milestone h2Milestone;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String targetUser = request.getParameter("userName");
        //System.out.println(targetUser);
        String loggedUser = user(request);
        Map<String, Object> map = baseMap(request);
        //List<SharedUsername> sharedUsers = h2Milestone.getSharedUserNames(loggedUser);
        map.put("sharedBy", targetUser);
        //map.put("sharedUsers",sharedUsers);
        if (loggedUser == null) {
            //System.out.println("No user");
            response.sendRedirect("/loginServlet");
        } else {
            if (targetUser != null) {
                if (h2Milestone.getSharedUser(targetUser, loggedUser)) {
                    List<Milestone> milestones = h2Milestone.findMilestone(targetUser);
                    if (milestones != null) {
                        map.put("milestones", milestones);
                    }
                }
                showView(response, SHARED_MILESTONES_TEMPLATE, map);
            } else{
                response.sendRedirect(response.encodeRedirectURL("/milestone"));
            }

        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            System.out.println("Hello, I'm a post method");

    }

}
