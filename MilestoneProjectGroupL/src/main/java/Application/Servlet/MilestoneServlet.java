package Application.Servlet;

import Application.H2Milestone;
import Application.Model.Milestone;
import Application.Model.SharedUsername;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class MilestoneServlet extends BaseServlet {

    private static final String MILESTONES_TEMPLATE = "milestones.mustache";

    private final H2Milestone h2Milestone;

    public MilestoneServlet(H2Milestone h2Milestone) {
        this.h2Milestone = h2Milestone;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String user = user(request);
        if(user == null){
            //System.out.println("No user");
            response.sendRedirect("/loginServlet");
        } else{
            List<SharedUsername> sharedUsers = h2Milestone.getSharedUserNames(user);
            //System.out.println(sharedUsers);
            List<Milestone> milestones = h2Milestone.findMilestone(user);
            Map<String,Object> map = baseMap(request);
            map.put("sharedUsers",sharedUsers);
            //System.out.println(map.toString());
            map.put("milestones", milestones);
            //System.out.println(map.toString());
            showView(response, MILESTONES_TEMPLATE, map);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //String method = getString(request, METHOD_PARAMETER, "post");
        if ("Delete Selected".equals(request.getParameter("method"))) {
            if(request.getParameter("msgId") != null) {
                h2Milestone.deleteMilestone(request.getParameter("msgId"));
                //System.out.println("deleting id: " + request.getParameter(ID_PARAMETER));
                response.sendRedirect(response.encodeRedirectURL("/milestone"));
            }else {
                System.out.println("Nothing selected to delete");
                response.sendRedirect(response.encodeRedirectURL("/milestone"));
            }
        }
        else if ("Edit Selected".equals(request.getParameter("method"))) {
            System.out.println("editing id: " + request.getParameter("msgId"));
            //redirect(request, request.getParameter(ID_PARAMETER));
            if(request.getParameter("msgId") != null) {
                HttpSession session = request.getSession(true);
                session.setAttribute("milestoneId", request.getParameter("msgId"));
                response.sendRedirect(response.encodeRedirectURL("/editMilestone"));
            }else{
                System.out.println("Nothing selected to edit");
                response.sendRedirect(response.encodeRedirectURL("/milestone"));
            }
        }
        else if ("Complete Milestone".equals(request.getParameter("method"))) {
            //System.out.println("editing id: " + request.getParameter(ID_PARAMETER));
            //redirect(request, request.getParameter(ID_PARAMETER));
            if(request.getParameter("msgId") != null) {
                h2Milestone.completeMilestone(request.getParameter("msgId"));
                //System.out.println("deleting id: " + request.getParameter(ID_PARAMETER));
                response.sendRedirect(response.encodeRedirectURL("/milestone"));
            }else{
                System.out.println("Nothing selected");
                response.sendRedirect(response.encodeRedirectURL("/milestone"));
            }
        }
        else if("Create Milestone".equals(request.getParameter("method"))) {
            String user = user(request);
            String title = request.getParameter("title");
            String description = request.getParameter("description");
            String duedate = request.getParameter("duedate");
            String completiondate = request.getParameter("completiondate");

            Milestone m = new Milestone(title, description, duedate, completiondate);
            h2Milestone.addMilestone(user, m);
            response.sendRedirect(response.encodeRedirectURL("/milestone"));
        }
        else if ("Delete Shared User".equals(request.getParameter("method"))) {
            if (request.getParameter("shareId") != null) {
                System.out.println(request.getParameter("shareId"));
                h2Milestone.deleteSharedUser(request.getParameter("shareId"));
                response.sendRedirect(response.encodeRedirectURL("/milestone"));
            } else {
                System.out.println("Nothing selected to delete");
                response.sendRedirect(response.encodeRedirectURL("/milestone"));
            }
        }
        else if("Share With User".equals(request.getParameter("method"))) {
            System.out.println("got here");
            String shareWithUser = request.getParameter("user");
            if(!shareWithUser.equals("")){
                try {
                    h2Milestone.addSharedUser(user(request),shareWithUser);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                response.sendRedirect(response.encodeRedirectURL("/milestone"));
            } else{
                System.out.println("No username added");
                response.sendRedirect(response.encodeRedirectURL("/milestone"));
            }
        }

    }


    //sets the re-direct attribute in the session
    protected void redirect(HttpServletRequest request, String id) {
        HttpSession session = request.getSession(true);
        session.setAttribute("milestoneId", id);
    }


}
