package Application;

import Application.Model.Milestone;
import Application.Login.PasswordHash;
import Application.Model.SharedUsername;

import java.io.IOException;
import java.sql.*;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import java.util.*;


public class H2Milestone implements AutoCloseable {

    public static final String MEMORY = "jdbc:h2:mem:shop";
    public static final String FILE = "jdbc:h2:~/Milestones";

    private Connection connection;

    static Connection getConnection(String db) throws SQLException, ClassNotFoundException {
        Class.forName("org.h2.Driver");  // ensure the driver class is loaded when the DriverManager looks for an installed class. Idiom.
        return DriverManager.getConnection(db, "sa", "");  // default password, ok for embedded.
    }

    public H2Milestone() {

        this(FILE);
    }

    public H2Milestone(String db) {

        try {

            connection = getConnection(db);
            loadResource("/milestones.sql");
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public void addMilestone(String userId, Milestone milestone) {
        final String ADD_MILESTONE_QUERY = "INSERT INTO milestones (username, title, description, duedate, completiondate,id) " +
                "VALUES (?,?,?,?,?,?)";
        try (PreparedStatement ps = connection.prepareStatement(ADD_MILESTONE_QUERY)) {
            ps.setString(1, userId);
            ps.setString(2, milestone.getTitle());
            ps.setString(3, milestone.getDescription());
            ps.setString(4, milestone.getDueDate());
            ps.setString(5, milestone.getCompletionDate());
            ps.setString(6,milestone.getId());
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Milestone> findMilestone(String userId) {
        final String LIST_USERS_QUERY = "SELECT username, title, description, duedate, completiondate, id  " +
                "FROM milestones WHERE username=?";
        List<Milestone> out = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(LIST_USERS_QUERY)) {
            ps.setString(1, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                out.add(new Milestone(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5),rs.getString(6)));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return out;
    }

    public void deleteMilestone(String id) {
        final String DELETE_MILESTONE_QUERY = "DELETE FROM milestones WHERE id=?";
        try (PreparedStatement ps = connection.prepareStatement(DELETE_MILESTONE_QUERY)) {
            ps.setString(1, id);
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void updateMilestone(String milestoneId, Milestone milestone) {

        final String UPDATE_MILESTONE_QUERY = "UPDATE milestones SET title =?, description=?, duedate=?, completiondate=? " +
                "WHERE id=?";
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_MILESTONE_QUERY)) {

            ps.setString(1, milestone.getTitle());
            ps.setString(2, milestone.getDescription());
            ps.setString(3, milestone.getDueDate());
            ps.setString(4, milestone.getCompletionDate());
            ps.setString(5, milestoneId);
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Milestone getMilestone(String id) {
        final String GET_MILESTONE_QUERY = "SELECT username, title, description, duedate, completiondate, id  " +
                "FROM milestones WHERE id=?";
        Milestone milestone = new Milestone();
        try (PreparedStatement ps = connection.prepareStatement(GET_MILESTONE_QUERY)) {
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                milestone = new Milestone(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5),rs.getString(6));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
       }
        return milestone;
    }

    public synchronized boolean register(final String userName, final String password) {
        errIfClosed();
        try {
            return registerSQL(userName, password);
        } catch (SQLException e) {

            return false;
        }
    }

    public List<SharedUsername> getSharedUserNames(String userName){
        List<SharedUsername> out = new ArrayList<>();
        final String GET_SHARED_USERS = "SELECT sharedWithUser FROM sharedWith WHERE username=?";
        try (PreparedStatement ps = connection.prepareStatement(GET_SHARED_USERS)) {
            ps.setString(1, userName);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                out.add(new SharedUsername(rs.getString(1)));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
            return out;

    }

    private boolean registerSQL(String userName, String password) throws SQLException {

        String hash = hash(password);

        if (hash == null) {
            return false;
        }
        if (hasUserSQL(userName)) {
            return false;
        }
        String query = "INSERT into users (username, password) VALUES(?,?)";

        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, userName);
            ps.setString(2, hash);
            int count = ps.executeUpdate();

            return count == 1;
        }

    }




    boolean hasUserSQL(String userName) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement("SELECT username FROM users WHERE username = ?")) {
            ps.setString(1, userName);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        }
    }




    public synchronized boolean login(final String userName, final String password) {
        errIfClosed();

        try {
            return loginSQL(userName, password);
        } catch (SQLException e) {

            return false;
        }
    }


    private boolean loginSQL(String userName, String password) throws SQLException {

        try (PreparedStatement ps = connection.prepareStatement("SELECT password FROM users WHERE username = ?")) {
            ps.setString(1, userName);
            ResultSet rs = ps.executeQuery();


            if (rs.next()) {

                String hash = rs.getString("password");
                return (hash == null) ? false : validate(password, hash);
            }
        }
        return false;
    }


    private boolean validate(String password, String hash) {
        try {
            return PasswordHash.validatePassword(password, hash);
        } catch (PasswordHash.PasswordException e) {

            return false;
        }
    }


    private String hash(String password) {
        try {
            return PasswordHash.createHash(password);
        } catch (PasswordHash.PasswordException e) {

            return null;
        }
    }

    private void loadResource(String name) {
        try {

            String cmd = new Scanner(getClass().getResource(name).openStream()).useDelimiter("\\Z").next();

            PreparedStatement ps = connection.prepareStatement(cmd);
            ps.execute();
        } catch (SQLException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void errIfClosed() {
        if (connection == null) {
            throw new NullPointerException("H2 connection is closed");
        }
    }



    public void completeMilestone(String milestoneId){
        final String UPDATE_MILESTONE_QUERY = "UPDATE milestones SET completiondate=? WHERE id=?";
        String timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        System.out.println(timeStamp);
        try (PreparedStatement ps = connection.prepareStatement(UPDATE_MILESTONE_QUERY)) {


            ps.setString(1, timeStamp);
            ps.setString(2, milestoneId);
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }




    public void addSharedUser(String userName, String sharedWithUser)throws SQLException {

        if(hasUserShared(userName)){
            System.out.print("User has already shared");
        }else {
            final String ADD_MILESTONE_QUERY = "INSERT INTO sharedWith (username, sharedWithUser) VALUES (?,?)";
            try (PreparedStatement ps = connection.prepareStatement(ADD_MILESTONE_QUERY)) {
                ps.setString(1, userName);
                ps.setString(2, sharedWithUser);
                ps.execute();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void deleteSharedUser(String sharedWithUser) {
        final String DELETE_SHARED_USER_QUERY = "DELETE FROM sharedWith WHERE sharedWithUser=?";
        try (PreparedStatement ps = connection.prepareStatement(DELETE_SHARED_USER_QUERY)) {
            ps.setString(1, sharedWithUser);
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public boolean getSharedUser(String userName, String targetUser) {
        final String GET_SHARED_USER_QUERY = "SELECT id FROM sharedWith WHERE (username=? AND sharedWithUser =?) ";

        try (PreparedStatement ps = connection.prepareStatement(GET_SHARED_USER_QUERY)) {
            ps.setString(1, userName);
            ps.setString(2, targetUser);
            ResultSet rs = ps.executeQuery();
            //System.out.println(rs.toString());
            return rs.next();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    boolean hasUserShared(String userName) throws SQLException {
        try (PreparedStatement ps = connection.prepareStatement("SELECT username FROM sharedWith WHERE username = ?")) {
            ps.setString(1, userName);
            ResultSet rs = ps.executeQuery();
            return rs.next();
        }
    }

}



