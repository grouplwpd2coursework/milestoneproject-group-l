package Application.Model;

import java.util.UUID;

public class Milestone{

private String userId;
private String title;
private String description;
private String dueDate;
private String completionDate;
private String id;



//Constuctor for both no data and all data being passed
    public Milestone() {
        this.title = "";
        this.description = "";
        this.dueDate = "";
        this.completionDate = "";
        //this.id = UUID.randomUUID().toString();
    }



    public Milestone(String title, String description, String dueDate, String completionDate){
        this.title = title;
        this.description = description;
        this.dueDate = dueDate;
        this.completionDate = completionDate;
        //this.id = UUID.randomUUID().toString();
    }

    public Milestone(String userId, String title, String description, String dueDate, String completionDate, String id){
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.dueDate = dueDate;
        this.completionDate = completionDate;
        this.id = id;
        //System.out.println(id);
    }

//    public Milestone(String userId, String title, String description, String dueDate, String id){
//        this.userId = userId;
//        this.title = title;
//        this.description = description;
//        this.dueDate = dueDate;
//        this.id = id;
//    }


//Getters and setters


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public String getId(){
        return this.id;
    }



    //To String method


    @Override
    public String toString() {
        return "Milestone{" +
                "userId='" + userId + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", completionDate='" + completionDate + '\'' +
                ", completionDate='" + id + '\'' +
                '}';
    }
}
