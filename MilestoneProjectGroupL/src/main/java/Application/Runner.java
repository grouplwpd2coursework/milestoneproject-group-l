package Application;


import Application.Servlet.*;
import Application.Login.User;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;


public class Runner {



    private final H2Milestone h2Milestone;
    private static final int PORT = 9001;

    private Runner() {


        h2Milestone = new H2Milestone();

    }

    private void start() throws Exception {

        Server server = new Server(PORT);


        /*
        servlet handler controls the context, ie where web resources are located.
         */
        ServletContextHandler handler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);
        handler.setInitParameter("org.eclipse.jetty.servlet.Default." + "resourceBase", "MilestoneProjectGroupL/src/main/resources/webapp");

        /*
        Servlet to handle index page.
         */
        IndexServlet indexServlet = new IndexServlet();
        handler.addServlet(new ServletHolder(indexServlet), "/indexServlet");

        LoginServlet login = new LoginServlet(h2Milestone);
        handler.addServlet(new ServletHolder(login), "/loginServlet");

        MilestoneServlet milestone = new MilestoneServlet(h2Milestone);
        handler.addServlet(new ServletHolder(milestone), "/milestone");

        EditMilestoneServlet editMilestone = new EditMilestoneServlet(h2Milestone);
        handler.addServlet(new ServletHolder(editMilestone), "/editMilestone");

        LogoutServlet logout = new LogoutServlet();
        handler.addServlet(new ServletHolder(logout), "/logout");

        SharedMilestonesServlet shared = new SharedMilestonesServlet(h2Milestone);
        handler.addServlet(new ServletHolder(shared), "/shared");

        /*
        sets default servlet path.
         */
        DefaultServlet ds = new DefaultServlet();
        handler.addServlet(new ServletHolder(ds), "/");


        /*
        starts server
         */
        server.start();
        System.out.println("Server started, will run until terminated");
        server.join();
    }

    /*
    main program start here
    */
    public static void main(String[] args) {
        try {
            System.out.println("starting");
            new Runner().start();
        } catch (Exception e) {
            System.out.println("Unexpected error: " + e.getMessage());
        }
    }




}
