CREATE TABLE IF NOT EXISTS users (
  username VARCHAR(256) PRIMARY KEY,
  password VARCHAR(256),
);

CREATE TABLE IF NOT EXISTS milestones (
  id int AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(256),
  title VARCHAR(4096),
  description VARCHAR(4096),
  duedate VARCHAR(1028),
  completiondate VARCHAR(4096),
  //messageId VARCHAR(4096)
  FOREIGN KEY (username) REFERENCES users(username)
);

CREATE TABLE IF NOT EXISTS sharedWith (
  id int AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(256),
  sharedWithUser VARCHAR(256),
  FOREIGN KEY (username) REFERENCES users(username)
  );


